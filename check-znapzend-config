#!/usr/bin/perl

use warnings;
use strict;

open my $fh, "-|", "zfs list -H -o name"
    or die "$!";

my @datasets = <$fh>;

close $fh
    or die "$!";

my %required_properties = (
    'org.znapzend:tsformat'     => '%Y-%m-%d-%H%M%SZ',
    'org.znapzend:mbuffer_size' => '1G',
    'org.znapzend:recursive'    => 'off',
    'org.znapzend:src_plan' =>
        '7days=>1hours,90days=>1days',
    'org.znapzend:enabled'       => 'on',
    'org.znapzend:pre_znap_cmd'  => 'off',
    'org.znapzend:mbuffer'       => 'off',
    'org.znapzend:post_znap_cmd' => 'off',
);

DATASET:
for my $dataset (@datasets) {
    chomp $dataset;

    my %properties;

    open $fh, "-|", "zfs get -H -s local -o property,value all $dataset"
        or die "$!";

SET_PROPERTIES:
    while ( my $line = <$fh> ) {
        my ( $property, $value ) = $line =~ m/^ ([^\s]+) \s+ ([^\s]+) $/msx
            or die "cannot parse output from zfs get";

        next SET_PROPERTIES
            if ( !defined $required_properties{$property} );

        $properties{$property} = $value;
    }

    close $fh
        or die "$!";

    open $fh, "-|", "zfs get -H -o value ch.kzone:znapzend $dataset"
        or die "$!";

    my $value = <$fh>;
    chomp $value;

    close $fh
        or die "$!";

    next DATASET
        if ( $value eq 'off' );

    if ( ( $value ne 'on' ) and ( $value ne '-' ) ) {
        warn
            "'ch.kzone:znapzend' is set to '$value' but it must either be 'on', 'off' or undefined.";

        # fallthrough - default to on
    }

    $properties{'ch.kzone:znapzend'} = 'on';

    next DATASET
        if ( $properties{'ch.kzone:znapzend'} eq 'off' );

PROPERTIES:
    foreach my $property ( keys %required_properties ) {
        if ( !defined $properties{$property} ) {
            print
                "WARNING: Missing property '$property' on dataset '$dataset'\n";
            last PROPERTIES;
        }

        my $value          = $properties{$property};
        my $expected_value = $required_properties{$property};

        if ( $value ne $expected_value ) {
            print
                "WARNING: Property '$property' on dataset '$dataset' is '$value' but should be '$expected_value'\n";
            next PROPERTIES;
        }
    }
}

