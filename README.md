check-znapzend-config
=====================

Check that all datasets have the correct configuration for znapzend.

The custom property 'ch.kzone:znapzend' can be defined to 'off' if a dataset
should not be configured for znapzend.
